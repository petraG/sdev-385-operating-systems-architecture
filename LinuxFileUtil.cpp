/*******************************************************************************
   SDEV-385-81: OS Architecture
   Petra Gates
   Wk10 Assignment: Linux File Utility Program
   11/17/2020

    Instructions:  The systems administrator at your company has asked you to
    write a file utility program that gives useful information about the file
    system on the company server. Your program should include as a minimum:
        - A menu-driven or GUI interface (your choice)
        - Thorough commenting
        - Error handling
        - The ability to select a file or files to view
        - The ability to select specific information about a file(s) to display
        - The ability to change the permissions on a file or files
        - The ability to copy a file to a new directory
******************************************************************************/

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/sysmacros.h>
#include <iostream>
#include <string>

using namespace std;

string userDirInput, userFileInput, filePath;          // global variables for user input

  /////////////////////////////////////
 ///     Functions Prototypes      ///
/////////////////////////////////////
void initiateInterface();                   // interface function prototype
void enterInfo();                           // file path entry function prototype
void viewFileContents();                    // menu(1) function
void fileInfoSubmenu();                     // menu(2) function
void changeFilePermissions();               // menu(3) function
void copyFile();                            // menu(4) function

int main (int argc, char **argv) {
    enterInfo();                            // calling file path input function

    while (true) {                          // menu while loop
        initiateInterface();                // calling interface function
    }

    return 0;
}

  /////////////////////////////////////
 ///     Functions Definitions     ///
/////////////////////////////////////

/************************************
*      Function: Getting File Path  *
* **********************************/
void enterInfo() {
    struct stat buffer;
    const char *listFilesCom;

    printf("\nEnter a complete directory path: ");
    cin >> userDirInput;                                           // input directory path

    if (stat(userDirInput.c_str(), &buffer) == 0)                  // checking dir validity
    {
        string listFiles = "ls " + userDirInput;                   // listing files in specified path
        listFilesCom = listFiles.c_str();
        system(listFilesCom);                                       // system command ls...

        printf("Enter the name of a file in this directory: ");
        cin >> userFileInput;

        filePath = userDirInput + "/" + userFileInput;         // concat dir and file -> path
        
        if (stat(filePath.c_str(), &buffer) != 0)                   // checking file validity
        //   && S_ISREG(buffer.st_mode) == 0
        {
            printf("\nInvalid filename.\n\n");
            system(listFilesCom);
            printf("Enter a valid filename: ");
            cin >> userFileInput;
            filePath = userDirInput + "/" + userFileInput;
        }    
    } else {
        printf("Invalid directory path");
        enterInfo();
    }
}

/*******************************************
 *  Function: Menu & Option Implementation *
* ****************************************/
void initiateInterface() {


    /************************************
    *            Menu Variables        *
    * **********************************/
    int mainMenuSelect;                     // main menu option var

    printf("\nOption Menu\n");
    printf("-----------------------------\n");
    printf("1) View file contents\n");
    printf("2) Choose file info to view\n");
    printf("3) Change file permissions for all users\n");
    printf("4) Copy file to new directory\n");
    printf("5) Specifiy new file\n");
    printf("0) Exit\n");

    printf("\nEnter a menu selection: ");                   // soliciting user menu option input
    cin >> mainMenuSelect;


    /************************************
     *  Switch: Option Implementation   *
     * *********************************/
    switch(mainMenuSelect) {
        case 0 :
            exit(1);

        /*****************************
         *     View File Contents    *
         * ***************************/
        case 1 :
        {
            viewFileContents();
            break;
        }

        /**********************************
         *     Enter File Info Submenu    *
         * ********************************/
        case 2 :
        {
            fileInfoSubmenu();
            break;
        }

        /**********************************
         *     Change File Permissions    *
         * ********************************/
        case 3 :
        {
            changeFilePermissions();
            break;
        }

        /**********************************
         *     Copy File to Directory     *
         * ********************************/
        case 4 :
        {
            copyFile();
            break;
        }

        /**********************************
         *          Specifiy File         *
         * ********************************/
        case 5 :                                                           // call function to enter directory and file info
        {
            enterInfo();
            break;
        }

        default :                                                          // menu option input error
        {
            printf("Invalid entry\n\n");
            break;
        }
    }
}

/***************************************
 *   Function Def: View File Contents  *
* *************************************/
void viewFileContents()
{
    cout << "Contents of " << userFileInput << ":" << endl;
    string viewFile = "cat " + filePath;
    const char *viewFileCom = viewFile.c_str();
    system(viewFileCom);                                            // system command cat
}

/***************************************
 *   Function Def: File Info Submenu   *
* *************************************/
void fileInfoSubmenu()
{
    char fileInfoSelect;                                // file info options variable for submenu
    mode_t theMode;                                     // holds permission mode stat
    struct stat theBuffer;                              // holds file info
    const char *theFileName;                            // char pointer to pass to file open
    int theFileDescriptor;                              // int for error handling

    theFileName = filePath.c_str();                            // assigning converted char pointer
    printf("\nFile: %s\n", theFileName);

    if((theFileDescriptor=open(theFileName,O_RDONLY)) < 0)       // open file
    {
        perror("Fail on Open");
        exit(EXIT_FAILURE);
    }

    if ((fstat(theFileDescriptor, &theBuffer)) < 0)            // get file stats
    {
        perror("Unable to get Statistics");
        exit(EXIT_FAILURE);
    }

    theMode = theBuffer.st_mode;                               // get mode info from stat struct buffer

    /********************
     *    Submenu       *
    * *****************/
    while (fileInfoSelect != 'e') {
        printf("file info submenu:\n");
        printf("----------------------\n");
        printf("a) Size\n");
        printf("b) Device\n");
        printf("c) Mode\n");
        printf("d) UID\n");
        printf("e) Return to main menu\n");
        printf("\nSelect info to view: ");
        cin >> fileInfoSelect;

        switch(fileInfoSelect) {
            case 'a' :                                                             // file size
                printf("The Size: %ld bytes\n\n", theBuffer.st_size);
                break;

            case 'b' :                                                              // file device
                printf("The Device (major): %i\n\n", major(theBuffer.st_dev));
                break;

            case 'c' :                                                              // file mode
                printf("The Mode: %#o\n\n", theMode & ~(S_IFMT));
                break;

            case 'd' :                                                              // file UID
                printf("The UID: %d\n\n", theBuffer.st_uid);
                break;

            case 'e' :
                break;

            default :
                printf("\nInvalid entry\n\n");
                break;
        }
    }

    if(close(theFileDescriptor) < 0)                                  // close file
    {
        perror("Fail on Close");
        // break;
    }
}

/***************************************
 *   Function Def: Change Permissions  *
* *************************************/
void changeFilePermissions()
{
    char permYesNo;                         // submenu permission change var
    string perms = "";                      // string accumulator for permission change command
    
    printf("Would you like all users to have read access (y/n)?\n");         // read permission
    cin >> permYesNo;
    if (permYesNo == 'y' || permYesNo == 'Y')
    {
        perms = perms + "r";
    }

    printf("Would you like all users to have write access (y/n)?\n");        // write permission
    cin >> permYesNo;
    if (permYesNo == 'y' || permYesNo == 'Y') {
        perms = perms + "w";
    }

    printf("Would you like all users to have execute access (y/n)?\n");      // execute permission
    cin >> permYesNo;
    if (permYesNo == 'y' || permYesNo == 'Y') {
        perms = perms + "x";
    }

    string permChange = "sudo chmod a=" + perms + " " + filePath;
    const char *permChangeCom = permChange.c_str();

    printf("\nDo you want to run the following command (y/n)?\n");           // confirm permission change
    printf("%s\n", permChangeCom);
    cin >> permYesNo;
    if (permYesNo == 'y' || permYesNo == 'Y')
    {
        system(permChangeCom);                                               // system call to chmod...
    }
}

/******************************
 *   Function Def: Copy File  *
* ****************************/
void copyFile()
{
    string destPath = "";                   // destination path var for file copy

    printf("Enter the directory path to copy the file to: ");
    cin >> destPath;

    string dirPerm = "sudo chmod -R a+rw " + destPath;               // granting write permission to directory
    const char *dirPermCom = dirPerm.c_str();
    system(dirPermCom);

    string copyFile = "sudo install -C -m 706 " + filePath + " " + destPath;    // using install to copy and
    const char *copyFileCom = copyFile.c_str();                                 // modify permissions mode
    system(copyFileCom);                                                        // system call to install...
}

/*******************************
 *          NOTES:             *
 * *****************************/
